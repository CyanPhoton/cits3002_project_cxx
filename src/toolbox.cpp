/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

#include "toolbox.h"

#include <sstream>
#include <chrono>

const char *server_name = "NO NAME SET";

std::pair<std::string, std::string> split_string(const std::string &input, const std::string &delim) {
    std::string output1;
    std::string output2;

    auto split = input.find(delim);
    if (split == std::string::npos) {
        return {input, ""};
    }

    output1 = input.substr(0, split);
    output2 = input.substr(split + delim.length());

    return {output1, output2};
}

std::vector<std::string> split_string_all(const std::string &input, const std::string &delim) {
    std::vector<std::string> output;

    std::string remainder = input;
    while (!remainder.empty()) {
        std::string part;
        std::tie(part, remainder) = split_string(remainder, delim);
        if (part.empty()) {
            if (!remainder.empty()) {
                output.push_back(remainder);
            }
            break;
        }
        output.push_back(part);
    }

    return output;
}

uint16_t get_now_time_int() {
    std::time_t t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::tm *now = std::localtime(&t);
    uint16_t clock_time_int = now->tm_min + now->tm_hour * 60;

    return clock_time_int;
}

uint16_t clock_24h_time_to_int(const std::string &clock_time) {
    auto parts = split_string(clock_time, ":");
    uint16_t result = std::stoi(parts.second);
    result += std::stoi(parts.first) * 60;

    return result;
}

std::string int_to_clock_12h_time(uint16_t int_time) {
    uint16_t minutes = int_time % 60;
    uint16_t hours = (int_time - minutes) / 60;

    bool is_pm = false;
    if (hours >= 12) {
        is_pm = true;
        if (hours >= 13) {
            hours -= 12;
        }
    }

    std::stringstream output;
    output << hours << (minutes < 10 ? ":0" : ":") << minutes << (is_pm ? "pm" : "am");

    return output.str();
}

std::string form_response_immediate(const TimetableEntry &first_timetable, uint16_t request_time) {
    std::stringstream result;

    if (request_time > first_timetable.departure_time) {
        // Departure time is next day

        result << "There is no path to " << first_timetable.destination_name << " that leaves today, the first path tomorrow is:<br>";
    }

    result << "First leg: "
           << first_timetable.transport_mode
           << " from "
           << first_timetable.location
           << " leaving at: "
           << int_to_clock_12h_time(first_timetable.departure_time)
           << " and arriving at: "
           << int_to_clock_12h_time(first_timetable.arrival_time);

    return result.str();
}

std::string form_response_general(const TimetableEntry &first_timetable, uint16_t request_time, uint16_t arrival_time, const std::string &destination_name, uint16_t day_jumps) {
    std::stringstream result;

    if (request_time > first_timetable.departure_time) {
        // Departure time is next day

        result << "There is no path to " << destination_name << " that leaves today, the first path tomorrow is:<br>";
    } else if (day_jumps > 0) {
        // Leave today, but don't get there today

        result << "There is no path to " << destination_name << " that arrives today, the first path leaving today is:<br>";
    }

    result << "First leg: "
           << first_timetable.transport_mode
           << " from "
           << first_timetable.location
           << " leaving at: "
           << int_to_clock_12h_time(first_timetable.departure_time)
           << (day_jumps > 0 ? (request_time > first_timetable.departure_time ? " Tomorrow" : " Today") : "") // Add specifying the depart day if needed
           << " and arriving to: "
           << destination_name
           << " at: "
           << int_to_clock_12h_time(arrival_time);

    if (day_jumps == 1) {
        result << " Tomorrow";
    } else if (day_jumps > 1) {
        result << " in " << day_jumps << " Days";
    }

    return result.str();
}
