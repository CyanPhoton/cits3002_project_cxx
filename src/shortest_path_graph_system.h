/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

#ifndef CITS3002_PROJECT_SHORTEST_PATH_GRAPH_SYSTEM_H
#define CITS3002_PROJECT_SHORTEST_PATH_GRAPH_SYSTEM_H

#include <unordered_set>
#include <unordered_map>
#include <vector>

#include "internode_udp.h"
#include "timetable.h"
#include "unique_low_priority_queue.h"

// Byte representation:                 | Byte offsets:
// traversal_id: 8 bytes                | 0
// depart_time: 2 bytes                 | 8 bytes
// path_len: 2 bytes                    | 10 bytes
// path_index: 2 bytes                  | 12 bytes
// path: (path_len) * 2 bytes           | 14 bytes
// dest_name_len: 2 bytes               | 14 + ((path_len) * 2) bytes
// dest_name: (dest_name_len) bytes     | 16 + ((path_len) * 2) bytes
struct NodeDataRequest {
    uint64_t traversal_id;
    std::string destination_name;
    std::vector<uint16_t> path{};
    uint16_t current_path_index = 0;
    uint16_t depart_time = 0;

    std::vector<uint8_t> to_data();
    static NodeDataRequest from_data(std::vector<uint8_t> data);
};

// Byte representation:                                 | Byte offsets:
// traversal_id: 8 bytes                                | 0 bytes
// path_len: 2 bytes                                    | 8 bytes
// path_index: 2 bytes                                  | 10 bytes
// path: (path_len) * 2 bytes                           | 12 bytes
// neighbour_len: 2 bytes                               | 12 + ((path_len) * 2) bytes
// neighbour_is_destination_index: 2 bytes              | 14 + ((path_len) * 2) bytes
// neighbour_uids: (neighbour_len) * 2 bytes            | 16 + ((path_len) * 2) bytes
// neighbour_arrival_times: (neighbour_len) * 2 bytes   | 16 + ((path_len) * 2) + ((neighbour_len) * 2) bytes
struct NodeDataResponse {
    uint64_t traversal_id;
    std::vector<uint16_t> path{};
    std::vector<uint16_t> unique_ids{};
    std::vector<uint16_t> arrival_times{};
    uint16_t current_path_index = 0;
    uint16_t is_dest_index = -1;

    std::vector<uint8_t> to_data();
    static NodeDataResponse from_data(std::vector<uint8_t> data);
};

struct TravelNode {
    // Path of unique ID's to take to reach node
    std::vector<uint16_t> path{};
    uint16_t arrival_time = -1;
    bool is_destination = false;
    uint16_t day_jumps = 0;
};

class ShortestPathTraversal {
    static uint64_t next_sub_id;

    uint64_t traversal_id;

    // Travel time, Unique ID
    unique_low_priority_queue<uint16_t, uint16_t> frontier{};
    // Unique ID of those visited
    std::unordered_set<uint16_t> visited{};
    // Unique ID -> Travel Node
    std::unordered_map<uint16_t, TravelNode> nodes{};

    uint16_t leave_time;

    std::string destination_name;

    std::optional<uint16_t> destination_node = {};

    bool waiting_on_response = false;
    bool no_more_requests = false;

    uint16_t last_request_node_uid = 0;
    uint16_t last_request_path_travel_time = 0;
public:
    ShortestPathTraversal(uint16_t unique_id, const std::string &name, const std::string &destination_name, uint16_t leave_time, const InternodeUDP &internode_udp, const Timetable &timetable);

    bool is_complete() const;
    std::optional<TravelNode> result() const;
    uint16_t get_start_time() const;
    const std::string &get_destination_name() const;
    uint64_t get_traversal_id() const;

    std::optional<NodeDataRequest> traverse(const std::optional<NodeDataResponse> &response);
};

#endif //CITS3002_PROJECT_SHORTEST_PATH_GRAPH_SYSTEM_H
