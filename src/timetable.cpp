/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

#include "timetable.h"

#include <sstream>
#include <fstream>
#include <algorithm>

#include "toolbox.h"

const char *Timetable::OPT_DIR = "timetables";

Timetable::Timetable(const std::string &name) {
    std::ifstream timetable_file("tt-" + name);
    if (!timetable_file.is_open()) {
        timetable_file = std::ifstream(std::string(OPT_DIR) + "/tt-" + name);

        EXPECT(timetable_file.is_open(), "Failed to located timetable file: tt-" << name)
    }

    std::stringstream file_ss;
    file_ss << timetable_file.rdbuf();
    std::string file_str = file_ss.str();

    auto lines = split_string_all(file_str, "\n");

    for (int i = 1; i < lines.size(); ++i) { // Skip first line of file

        auto sections = split_string_all(lines[i], ",");
        EXPECT(sections.size() == 5, "Got line in timetable file without 5 sections: " << lines[i])

        TimetableEntry timetableEntry{
                clock_24h_time_to_int(sections[0]),
                sections[1],
                sections[2],
                clock_24h_time_to_int(sections[3]),
                sections[4]
        };

        timetable_entries[timetableEntry.destination_name].push_back(timetableEntry);
    }

    // Sort each list from earliest arrival to latest, so that next day loop around is simply [0]
    for (auto& pair : timetable_entries) {
        std::sort(pair.second.begin(), pair.second.end(), [](const TimetableEntry& lhs, const TimetableEntry& rhs) {
            return lhs.departure_time < rhs.departure_time;
        });
    }
}

std::filesystem::file_time_type Timetable::get_last_modified(const std::string &name) {
    std::string path = "tt-" + name;

    if (!std::filesystem::exists(path)){
        path = std::string(OPT_DIR) + "/" + path;
        EXPECT(std::filesystem::exists(path), "Can not get the last modified time of non existent timetable file: tt-" << name)
    }

    return std::filesystem::last_write_time(path);
}

std::vector<std::string> Timetable::get_destinations() const {
    std::vector<std::string> result{};

    for (const auto& key_value : timetable_entries) {
        result.push_back(key_value.first);
    }

    return result;
}

std::optional<const std::vector<TimetableEntry>*> Timetable::get_timetable_entries_by_dest(const std::string &dest_name) const {
    if (timetable_entries.count(dest_name) > 0) {
        return {&timetable_entries.at(dest_name)};
    }
    return {};
}

std::optional<const TimetableEntry *> Timetable::get_earliest_timetable_entry_by_dest(const std::string &dest_name, uint16_t from_time) const {
    if (timetable_entries.count(dest_name) == 0) {
        return {};
    }
    const auto& entries = timetable_entries.at(dest_name);

    int last_valid = 0;
    uint16_t most_recent = -1;
    for (int i = 0; i < entries.size(); ++i) {
        if (entries[i].departure_time >= from_time && entries[i].arrival_time < most_recent) {
            last_valid = i;
            most_recent = entries[i].arrival_time;
        }
    }

    return &entries[last_valid];
}

const std::unordered_map<std::string, std::vector<TimetableEntry>> &Timetable::get_timetable_entries() const {
    return timetable_entries;
}
