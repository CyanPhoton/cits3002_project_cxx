/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

#ifndef CITS3002_PROJECT_TIMETABLE_H
#define CITS3002_PROJECT_TIMETABLE_H

#include <string>
#include <vector>
#include <unordered_map>
#include <optional>
#include <filesystem>

struct TimetableEntry {
    uint16_t departure_time; // Minutes past midnight
    std::string transport_mode; // Bus number / Train line
    std::string location; //Bus stand / Train platform
    uint16_t arrival_time; // Minutes past midnight
    std::string destination_name;
};

class Timetable {
    const static char *OPT_DIR;

    // Key is destination name
    std::unordered_map<std::string, std::vector<TimetableEntry>> timetable_entries{};
public:
    explicit Timetable(const std::string &name);

    static std::filesystem::file_time_type get_last_modified(const std::string &name);

    std::vector<std::string> get_destinations() const;
    // Can't have optional ref
    std::optional<const std::vector<TimetableEntry>*> get_timetable_entries_by_dest(const std::string &dest_name) const;
    std::optional<const TimetableEntry*> get_earliest_timetable_entry_by_dest(const std::string &dest_name, uint16_t from_time = 0) const;
    const std::unordered_map<std::string, std::vector<TimetableEntry>> &get_timetable_entries() const;
};

#endif //CITS3002_PROJECT_TIMETABLE_H
