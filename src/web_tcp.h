/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

#ifndef CITS3002_PROJECT_WEB_TCP_H
#define CITS3002_PROJECT_WEB_TCP_H

#include <string>
#include <optional>
#include <utility>
#include <vector>
#include <unordered_map>

#include <poll.h>

struct HTTPRequest {
    std::string header;

    explicit HTTPRequest(std::string header) : header(std::move(header)) {}
};

class WebTCPResponder;

class WebTCPListener {
    int socket_fd;

public:
    explicit WebTCPListener(const std::string &port);
    ~WebTCPListener();

    WebTCPListener(WebTCPListener const &) = delete;
    WebTCPListener &operator=(const WebTCPListener &) = delete;

    pollfd get_poll_struct();
    std::optional<std::pair<HTTPRequest, WebTCPResponder>> process_poll_struct(pollfd poll_struct);
};

class WebTCPResponder {
    int remote_socket_fd;

    friend class WebTCPListener;

    explicit WebTCPResponder(int remote_socket_fd);
public:
    // Returns true if all data was sent, false if connection closed before hand
    bool send_and_close(const std::string &status, const std::string &body);
};

class RequestParsing {
public:
    static std::string extract_get_uri(const std::string &header);
    static std::unordered_map<std::string, std::string> extract_get_key_value_pairs(const std::string &uri);
};

#endif //CITS3002_PROJECT_WEB_TCP_H
