/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

#include "shortest_path_graph_system.h"

#include <netinet/in.h>

#include "toolbox.h"

std::vector<uint8_t> NodeDataRequest::to_data() {
    std::vector<uint8_t> data{};

    uint64_t traversal_id_n = htobe64(traversal_id); // No explicit 64 network one, so use the fact that network_endian == big endian
    uint16_t depart_time_n = htons(depart_time);
    uint16_t path_len = htons(path.size());
    uint16_t path_index = htons(current_path_index);
    uint16_t *path_data = path.data(); // htons not needed as id's are constantly in network form, and their only use is for identity, as such their numeric interpretation doesn't matter
    uint16_t dest_name_len = htons(destination_name.size());
    const char *dest_name_data = destination_name.c_str();

    data.insert(data.end(), (uint8_t *) &traversal_id_n, ((uint8_t *) &traversal_id_n) + sizeof(traversal_id_n));
    EXPECT(data.size() == (8), "Data should be expected size")

    data.insert(data.end(), (uint8_t *) &depart_time_n, ((uint8_t *) &depart_time_n) + sizeof(depart_time_n));
    EXPECT(data.size() == (8 + 2), "Data should be expected size")

    data.insert(data.end(), (uint8_t *) &path_len, ((uint8_t *) &path_len) + sizeof(path_len));
    EXPECT(data.size() == (8 + 2 + 2), "Data should be expected size")

    data.insert(data.end(), (uint8_t *) &path_index, ((uint8_t *) &path_index) + sizeof(path_index));
    EXPECT(data.size() == (8 + 2 + 2 + 2), "Data should be expected size")

    data.insert(data.end(), (uint8_t *) path_data, ((uint8_t *) path_data) + (2 * path.size()));
    EXPECT(data.size() == (8 + 2 + 2 + 2 + (2 * path.size())), "Data should be expected size")

    data.insert(data.end(), (uint8_t *) &dest_name_len, ((uint8_t *) &dest_name_len) + sizeof(dest_name_len));
    EXPECT(data.size() == (8 + 2 + 2 + 2 + (2 * path.size()) + 2), "Data should be expected size")

    data.insert(data.end(), (uint8_t *) dest_name_data, ((uint8_t *) dest_name_data) + destination_name.size());
    EXPECT(data.size() == (8 + 2 + 2 + 2 + (2 * path.size()) + 2 + destination_name.size()), "Data should be expected size")

    return data;
}

#include <sstream>

NodeDataRequest NodeDataRequest::from_data(std::vector<uint8_t> data) {
    NodeDataRequest result{};

    uint64_t traversal_id = be64toh(*((uint64_t *) (&data[0]))); // No explicit 64 network one, so use the fact that network_endian == big endian
    uint16_t depart_time = ntohs(*((uint16_t *) (&data[8])));
    uint16_t path_len = ntohs(*((uint16_t *) (&data[10])));
    uint16_t path_index = ntohs(*((uint16_t *) (&data[12])));
    uint16_t *path_data = ((uint16_t *) (&data[14])); // ntohs not needed as id's are constantly in network form, and their only use is for identity, as such their numeric interpretation doesn't matter
    uint16_t dest_name_len = ntohs(*(((uint16_t *) (&data[14] + (2 * path_len)))));
    const char *dest_name_data = (char *) (((uint16_t *) (&data[16] + (2 * path_len))));

    result.traversal_id = traversal_id;
    result.depart_time = depart_time;
    result.destination_name.insert(result.destination_name.end(), dest_name_data, dest_name_data + dest_name_len);
    EXPECT(result.destination_name.size() == dest_name_len, "Data should be expected size")
    result.path.insert(result.path.end(), path_data, path_data + path_len);
    EXPECT(result.path.size() == path_len, "Data should be expected size")
    result.current_path_index = path_index;

    EXPECT(data == result.to_data(), "Data should not be corrupted by conversion")

    return result;
}

std::vector<uint8_t> NodeDataResponse::to_data() {
    std::vector<uint8_t> data{};

    EXPECT(unique_ids.size() == arrival_times.size(), "Number of arrival times must equal number of unique ids")

    uint64_t traversal_id_n = htobe64(traversal_id); // No explicit 64 network one, so use the fact that network_endian == big endian
    uint16_t path_len = htons(path.size());
    uint16_t path_index = htons(current_path_index);
    uint16_t *path_data = path.data(); // htons not needed as id's are constantly in network form, and their only use is for identity, as such their numeric interpretation doesn't matter
    uint16_t neighbour_len = htons(unique_ids.size());
    uint16_t neighbour_is_destination_index = htons(is_dest_index);
    uint16_t *neighbour_uids = unique_ids.data(); // htons not needed as id's are constantly in network form, and their only use is for identity, as such their numeric interpretation doesn't matter

    std::vector<uint16_t> network_arrival_times{};

    network_arrival_times.reserve(arrival_times.size());
    for (const auto arrival_time : arrival_times) {
        network_arrival_times.push_back(htons(arrival_time)); // htons needed as times are used for numeric purposes
    }

    uint16_t *neighbour_arrival_times = network_arrival_times.data();

    data.insert(data.end(), (uint8_t *) &traversal_id_n, ((uint8_t *) &traversal_id_n) + sizeof(traversal_id_n));
    EXPECT(data.size() == (8), "Data should be expected size")

    data.insert(data.end(), (uint8_t *) &path_len, ((uint8_t *) &path_len) + sizeof(path_len));
    EXPECT(data.size() == (8 + 2), "Data should be expected size")

    data.insert(data.end(), (uint8_t *) &path_index, ((uint8_t *) &path_index) + sizeof(path_len));
    EXPECT(data.size() == (8 + 2 + 2), "Data should be expected size")

    data.insert(data.end(), (uint8_t *) path_data, ((uint8_t *) path_data) + (2 * path.size()));
    EXPECT(data.size() == (8 + 2 + 2 + (2 * path.size())), "Data should be expected size")

    data.insert(data.end(), (uint8_t *) &neighbour_len, ((uint8_t *) &neighbour_len) + sizeof(neighbour_len));
    EXPECT(data.size() == (8 + 2 + 2 + (2 * path.size()) + 2), "Data should be expected size")

    data.insert(data.end(), (uint8_t *) &neighbour_is_destination_index, ((uint8_t *) &neighbour_is_destination_index) + sizeof(neighbour_is_destination_index));
    EXPECT(data.size() == (8 + 2 + 2 + (2 * path.size()) + 2 + 2), "Data should be expected size")

    data.insert(data.end(), (uint8_t *) neighbour_uids, ((uint8_t *) neighbour_uids) + (2 * unique_ids.size()));
    EXPECT(data.size() == (8 + 2 + 2 + (2 * path.size()) + 2 + 2 + (2 * unique_ids.size())), "Data should be expected size")

    data.insert(data.end(), (uint8_t *) neighbour_arrival_times, ((uint8_t *) neighbour_arrival_times) + (2 * arrival_times.size()));
    EXPECT(data.size() == (8 + 2 + 2 + (2 * path.size()) + 2 + 2 + (2 * unique_ids.size()) + (2 * arrival_times.size())), "Data should be expected size")

    return data;
}

NodeDataResponse NodeDataResponse::from_data(std::vector<uint8_t> data) {
    NodeDataResponse result{};

    uint64_t traversal_id = be64toh(*((uint64_t *) (&data[0]))); // No explicit 64 network one, so use the fact that network_endian == big endian
    uint16_t path_len = htons(*((uint16_t *) (data.data() + 8)));
    uint16_t path_index = htons(*((uint16_t *) (data.data() + 10)));
    uint16_t *path_data = ((uint16_t *) (data.data() + 12)); // ntohs not needed as id's are constantly in network form, and their only use is for identity, as such their numeric interpretation doesn't matter
    uint16_t neighbour_len = htons(*((uint16_t *) (data.data() + 12 + (2 * path_len))));
    uint16_t neighbour_is_destination_index = htons(*((uint16_t *) (data.data() + 14 + (2 * path_len))));
    uint16_t *neighbour_uids_data = ((uint16_t *) (data.data() + 16 + (2 * path_len)));
    uint16_t *neighbour_arrival_times_data = ((uint16_t *) (data.data() + 16 + (2 * path_len) + (2 * neighbour_len)));

    result.path.insert(result.path.end(), path_data, path_data + path_len);
    EXPECT(result.path.size() == path_len, "Data should be expected size")
    result.unique_ids.insert(result.unique_ids.end(), neighbour_uids_data, neighbour_uids_data + neighbour_len);
    EXPECT(result.unique_ids.size() == neighbour_len, "Data should be expected size")
    result.arrival_times.insert(result.arrival_times.end(), neighbour_arrival_times_data, neighbour_arrival_times_data + neighbour_len);
    EXPECT(result.arrival_times.size() == neighbour_len, "Data should be expected size")

    for (auto &arrival_time : result.arrival_times) {
        arrival_time = ntohs(arrival_time); // ntohs needed as times are used for numeric purposes
    }

    result.traversal_id = traversal_id;
    result.current_path_index = path_index;
    result.is_dest_index = neighbour_is_destination_index;

    EXPECT(data == result.to_data(), "Data should not be corrupted by conversion")

    return result;
}

uint64_t ShortestPathTraversal::next_sub_id = 1;

ShortestPathTraversal::ShortestPathTraversal(uint16_t unique_id, const std::string &name, const std::string &destination_name, uint16_t leave_time, const InternodeUDP &internode_udp, const Timetable &timetable)
        : leave_time(leave_time), destination_name(destination_name) {

    // Allows for 2^48 (> 200 trillion) traversals before we have issue, I've made worse assumptions, I could create am id re-use system if really needed
    traversal_id = (((uint64_t) unique_id) << 48) + next_sub_id;
    next_sub_id++;

    TravelNode node{
            {unique_id},
            0,
            destination_name == name,
            0
    };
    visited.insert(unique_id);
    nodes[unique_id] = node;

    if (destination_name == name) {
        destination_node = unique_id;
        no_more_requests = true;
        return;
    }

    for (const auto &neighbour_destination : timetable.get_destinations()) {
        if (internode_udp.has_port_wise_neighbour(neighbour_destination)) {
            uint16_t neighbour_uid = internode_udp.get_neighbour_unique_id_from_name(neighbour_destination);

            auto arrive_time = timetable.get_earliest_timetable_entry_by_dest(neighbour_destination, leave_time);

            TravelNode neighbour_node{
                    {unique_id, neighbour_uid},
                    static_cast<uint16_t>(arrive_time.value()->arrival_time),
                    destination_name == neighbour_destination,
                    0
            };
            nodes[neighbour_uid] = neighbour_node;

            uint16_t travel_time;
            if (arrive_time.value()->arrival_time > leave_time) {
                travel_time = arrive_time.value()->arrival_time - leave_time;
            } else {
                travel_time = (arrive_time.value()->arrival_time + 24 * 60) - leave_time;
                nodes[neighbour_uid].day_jumps++;
            }

            frontier.push(travel_time, neighbour_uid);
        } else {

            // use 0 for unique ID as, there is no server but I need to provide something

            auto arrive_time = timetable.get_earliest_timetable_entry_by_dest(neighbour_destination, leave_time);

            TravelNode neighbour_node{
                    {unique_id, 0},
                    static_cast<uint16_t>(arrive_time.value()->arrival_time),
                    destination_name == neighbour_destination,
                    0
            };
            nodes[0] = neighbour_node;

            uint16_t travel_time;
            if (arrive_time.value()->arrival_time > leave_time) {
                travel_time = arrive_time.value()->arrival_time - leave_time;
            } else {
                travel_time = (arrive_time.value()->arrival_time + 24 * 60) - leave_time;
                nodes[0].day_jumps++;
            }

            frontier.push(travel_time, 0);
        }
    }
}

bool ShortestPathTraversal::is_complete() const {
    return no_more_requests;
}

std::optional<TravelNode> ShortestPathTraversal::result() const {
    if (destination_node.has_value() && nodes.count(destination_node.value()) != 0) {
        return nodes.at(destination_node.value());
    }

    return {};
}

uint16_t ShortestPathTraversal::get_start_time() const {
    return leave_time;
}

const std::string &ShortestPathTraversal::get_destination_name() const {
    return destination_name;
}

uint64_t ShortestPathTraversal::get_traversal_id() const {
    return traversal_id;
}

std::optional<NodeDataRequest> ShortestPathTraversal::traverse(const std::optional<NodeDataResponse> &response) {
    if (is_complete()) {
        return {};
    }

    EXPECT(response.has_value() == waiting_on_response, "Shortest path got response when not expecting, or vis-versa")

    // Add from response
    if (response.has_value()) {
        waiting_on_response = false;
        const auto &response_val = response.value();

        for (auto index = 0; index < response_val.unique_ids.size(); index++) {
            auto uid = response_val.unique_ids[index];

            if (visited.count(uid) != 0) {
                // Skip adding already visited
                continue;
            }

            const auto &last_node = nodes[last_request_node_uid];

            auto path = last_node.path;
            path.push_back(uid);

            auto arrival_time = response_val.arrival_times[index];

            TravelNode node{
                    path,
                    arrival_time,
                    index == response_val.is_dest_index,
                    last_node.day_jumps
            };

            nodes[uid] = node;

            // This logic accounts for looping around to the next day, but assumes that no single jump is > 24h (seems reasonable)
            uint16_t travel_time;
            if (arrival_time >= last_node.arrival_time) {
                travel_time = (arrival_time - last_node.arrival_time) + last_request_path_travel_time;
            } else {
                // arrival_time is the next day, so add 24h worth of time
                nodes[uid].day_jumps++;
                travel_time = (arrival_time + (24 * 60) - last_node.arrival_time) + last_request_path_travel_time;
            }

            frontier.push(travel_time, uid);
        }
    }

    // Nothing to do if empty
    if (frontier.empty()) {
        no_more_requests = true;
        return {};
    }

    // get top
    auto node = frontier.top_pair();
    frontier.pop();

    // if destination, search is complete
    if (nodes[node.second].is_destination) {
        destination_node = node.second;
        waiting_on_response = false;
        no_more_requests = true;
        return {};
    }

    // Add to visited
    visited.insert(node.second);

    auto path = nodes[node.second].path;
    last_request_node_uid = node.second;
    last_request_path_travel_time = node.first;

    // else need to add it's neighbours, which are got via a request
    NodeDataRequest request{
            traversal_id,
            destination_name,
            path,
            1, // Start path to destination by going ot the [1] in the path, since this current server is [0] in path
            nodes[node.second].arrival_time
    };

    waiting_on_response = true;
    return request;
}