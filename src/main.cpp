/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

#include <iostream>
#include <sstream>
#include <tuple>
#include <chrono>
#include <cstring>
#include <netinet/in.h>

#include "toolbox.h"
#include "web_tcp.h"
#include "internode_udp.h"
#include "shortest_path_graph_system.h"

struct TraversalInstance {
    ShortestPathTraversal traversal;
    WebTCPResponder responder;
    std::shared_ptr<Timetable> timetable;
};

int main(int argc, char const *const *argv) {
    EXPECT(argc >= 4, "Not enough parameters!")

    // Load up arguments as name and ports
    std::string name = argv[1];
    SET_SERVER_NAME(argv[1]);
    std::string web_tcp_port = argv[2];
    std::string internode_udp_port = argv[3];

    std::cout << "Hello, World from (c++) Station: " << name << std::endl;
    LOG("TCP port: " << web_tcp_port << " | UDP port: " << internode_udp_port << std::endl);

    std::vector<std::string> neighbour_udp_ports{};
    neighbour_udp_ports.reserve(argc - 4);
    for (int i = 4; i < argc; i++) {
        std::string port_str = argv[i];
        if (std::strtol(port_str.c_str(), nullptr, 10) == 0) {
            // Port is not actually a port, can use == 0 since 0 would be invalid port anyway, complete way is to use endptr param
            continue;
        }

        neighbour_udp_ports.emplace_back(port_str);
        LOG("Adjacent UDP port (" << (i - 4) << "): " << port_str << std::endl);
    }

    // Use web tcp port in network presentation as a unique ID, not actually used as port, just a unique ID
    // specifying one on startup would be an alternative if not all localhost and thus ports not unique
    uint16_t unique_id = htons(std::stoi(web_tcp_port));

    auto timetable_last_modified = Timetable::get_last_modified(name);
    std::shared_ptr<Timetable> timetable = std::make_shared<Timetable>(name);

    // Create and setup tcp and udp ports, ready to receive
    WebTCPListener web_tcp_listener(web_tcp_port);
    InternodeUDP internode_udp(name, unique_id, internode_udp_port, neighbour_udp_ports);

    for (const auto &neighbour_port : neighbour_udp_ports) {
        LOG("Has neighbour: " << internode_udp.get_neighbour_name(neighbour_port) << " | with uid: " << internode_udp.get_neighbour_unique_id_from_port(neighbour_port) << std::endl);
    }

    // Used to allow for multiple requests to the same server at once
    std::unordered_map<uint64_t, TraversalInstance> traversal_instance_map{};

    while (true) {
        // Poll for if this server needs to do anything
        pollfd poll_fd_structs[] = {
                web_tcp_listener.get_poll_struct(),
                internode_udp.get_poll_struct(),
        };

        int number = poll(poll_fd_structs, 2, -1);
        EXPECT(number >= 0, "Failed to poll sockets")

        // Reload timetable if needed
        auto new_timetable_last_modified = Timetable::get_last_modified(name);
        if (new_timetable_last_modified > timetable_last_modified) {
            timetable_last_modified = new_timetable_last_modified;
            timetable = std::make_shared<Timetable>(name);
        }

        // Check to see if a tcp event happened, and if so process it
        auto listener_status = web_tcp_listener.process_poll_struct(poll_fd_structs[0]);

        if (listener_status.has_value()) {
            // There was a http request, so read it and prepare response
            HTTPRequest http_request = listener_status.value().first;
            WebTCPResponder responder = listener_status.value().second;

            // Get the destination from request
            std::string uri = RequestParsing::extract_get_uri(http_request.header);
            auto get_pairs = RequestParsing::extract_get_key_value_pairs(uri);

            // Get if has valid "to=X" in request
            if (!get_pairs["to"].empty()) {

                std::string destination = get_pairs["to"];
                LOG("Got query for destination: " << destination << std::endl);

                auto request_time = get_now_time_int();

                // Start traversal of network via shortest path algorithm
                ShortestPathTraversal traversal(unique_id, name, destination, request_time, internode_udp, *timetable);

                if (traversal.is_complete()) {
                    // If traversal is already complete you are at the destination

                    std::string response;
                    if (traversal.result().has_value()) {
                        response = "You are already at: " + destination;
                    } else {
                        response = "You can not reach: " + destination;
                    }

                    responder.send_and_close("200 OK", response);
                } else {
                    // Else, must start traversing
                    auto request = traversal.traverse({});

                    if (!request.has_value()) {
                        // The traversal ended, and made no request for more information so
                        // either (dest is adjacent AND that is quickest path) OR (there is no path)

                        std::string response;
                        if (traversal.result().has_value()) {
                            // Traversal has a result, so shortest path is already found

                            auto first_stop_uid = traversal.result()->path[1];

                            const auto &first_stop_name = internode_udp.get_neighbour_name_from_uid(first_stop_uid);

                            auto first_stop = timetable->get_earliest_timetable_entry_by_dest(first_stop_name, traversal.get_start_time());

                            response = form_response_immediate(*first_stop.value(), request_time);
                        } else {
                            // No way to get to destination (aka this server has no neighbours)

                            response = "You can not reach: " + destination;
                        }

                        responder.send_and_close("200 OK", response);
                    } else {
                        // The traversal made a request for more info, so we send that request onto it's destination, and store the traversal instance

                        auto request_data = request->to_data();

                        const auto &request_port = internode_udp.get_neighbour_port_from_uid(request->path[request->current_path_index]);

                        internode_udp.send_message(request_port, InternodeType::NodeDataRequest, request_data);

                        TraversalInstance traversal_instance{
                                traversal,
                                responder,
                                timetable // Timetable stored for once the shortest path is found and building up a message based on that path to present to client
                        };

                        traversal_instance_map.insert({traversal.get_traversal_id(), traversal_instance});
                    }
                }
            } else {
                // Just ignoring invalid requests, closing responder with 404
                responder.send_and_close("404 Not Found", "");

                ERR_LOG("IGNORING: Got request with invalid to= Get request, or possible a favicon request or something" << std::endl);
            }
        }

        // Check if there was udp event
        ReceivedInternode received_internode = internode_udp.process_poll_struct(poll_fd_structs[1]);
        for (const auto &msg : received_internode.internode_messages) {
            // If so process the message depending in type, ignoring those we don't expect to get

            switch (msg.header.internodeType) {
                case InternodeType::Undefined:
                    //LOG("IGNORING Received 'Undefined' internode packet with " << msg.data.size() << " bytes" << std::endl);
                    break;
                case InternodeType::ReadyToReceive:
                    //LOG("IGNORING Received 'ReadyToReceive' internode packet from port: " << msg.get_port_string() << " with " << msg.data.size() << " bytes" << std::endl);
                    break;
                case InternodeType::Greeting:
                    //LOG("IGNORING Received 'Greeting' internode packet with " << msg.data.size() << " bytes" << std::endl);
                    break;
                case InternodeType::NodeDataRequest: {
                    LOG("PROCESSING Received 'NodeDataRequest' internode packet with " << msg.data.size() << " bytes" << std::endl);
                    // A request for data about a node was received

                    NodeDataRequest request = NodeDataRequest::from_data(msg.data);

                    if (request.current_path_index == request.path.size() - 1) {
                        // Message meant for this server, send back response

                        // Build response
                        NodeDataResponse response{};
                        response.traversal_id = request.traversal_id;
                        response.path = request.path;

                        response.is_dest_index = -1;
                        for (const auto &destination_name : timetable->get_destinations()) {
                            // Go through all destinations immediately reachable

                            if (internode_udp.has_port_wise_neighbour(destination_name)) {
                                // If that destination has a port, aka there is a server for that destination

                                response.unique_ids.push_back(internode_udp.get_neighbour_unique_id_from_name(destination_name));

                                // Get the corresponding arrival time for the port
                                auto arrival_entry = timetable->get_earliest_timetable_entry_by_dest(destination_name, request.depart_time);
                                response.arrival_times.push_back(arrival_entry.value()->arrival_time);

                                // If this port is the destination, set the appropriate flag
                                if (destination_name == request.destination_name) {
                                    response.is_dest_index = response.unique_ids.size() - 1;
                                }
                            } else {
                                // Can get to, but has no server associated with it

                                // Only if this destination is the intended destination, add and set the appropriate flag
                                if (destination_name == request.destination_name) {
                                    response.unique_ids.push_back(0); // No next server exists, but this information is not needed by shortest path, still need to add something though

                                    // Get the corresponding arrival time for the destination
                                    auto arrival_entry = timetable->get_earliest_timetable_entry_by_dest(destination_name, request.depart_time);
                                    response.arrival_times.push_back(arrival_entry.value()->arrival_time);

                                    response.is_dest_index = response.unique_ids.size() - 1;
                                }
                            }
                        }
                        response.current_path_index = response.path.size() - 2; // Start path back by going next to the second last in path

                        const auto &next_port = internode_udp.get_neighbour_port_from_uid(response.path[response.current_path_index]);
                        internode_udp.send_message(next_port, InternodeType::NodeDataResponse, response.to_data());
                    } else {
                        // Message meant for another, send on to next

                        request.current_path_index++; // Go to next in path

                        const auto &next_port = internode_udp.get_neighbour_port_from_uid(request.path[request.current_path_index]);

                        internode_udp.send_message(next_port, InternodeType::NodeDataRequest, request.to_data());
                    }

                    break;
                }
                case InternodeType::NodeDataResponse: {
                    LOG("PROCESSING Received 'NodeDataResponse' internode packet with " << msg.data.size() << " bytes" << std::endl);

                    NodeDataResponse response = NodeDataResponse::from_data(msg.data);

                    if (response.current_path_index == 0) {
                        // Message meant for this server, inform shortest path traversal, potentially send another request

                        if (traversal_instance_map.count(response.traversal_id) != 0) {
                            TraversalInstance &traversal_instance = traversal_instance_map.at(response.traversal_id);

                            auto request = traversal_instance.traversal.traverse(response);

                            if (request.has_value()) {
                                // Another request to send

                                auto request_data = request->to_data();

                                const auto &request_port = internode_udp.get_neighbour_port_from_uid(request->path[request->current_path_index]);

                                internode_udp.send_message(request_port, InternodeType::NodeDataRequest, request_data);
                            } else {
                                // Done searching, response to browser

                                std::string response_str;
                                if (traversal_instance.traversal.result().has_value()) {

                                    auto arrival_time = traversal_instance.traversal.result()->arrival_time;
                                    auto first_stop_uid = traversal_instance.traversal.result()->path[1];

                                    const auto &first_stop_name = internode_udp.get_neighbour_name_from_uid(first_stop_uid);

                                    auto first_stop = traversal_instance.timetable->get_earliest_timetable_entry_by_dest(first_stop_name, traversal_instance.traversal.get_start_time());

                                    response_str = form_response_general(*first_stop.value(), traversal_instance.traversal.get_start_time(), arrival_time,
                                                                         traversal_instance.traversal.get_destination_name(),
                                                                         traversal_instance.traversal.result()->day_jumps);
                                    //TODO possibly give full path?
                                } else {
                                    response_str = "You can not reach: " + traversal_instance.traversal.get_destination_name();
                                }

                                traversal_instance.responder.send_and_close("200 OK", response_str);

                                traversal_instance_map.erase(response.traversal_id);
                            }
                        } else {
                            ERR_LOG("IGNORING Got node data response for unknown traversal ID." << std::endl);
                        }
                    } else {
                        // Message meant for another, send on to next

                        response.current_path_index--; // Go to previous in path

                        const auto &next_port = internode_udp.get_neighbour_port_from_uid(response.path[response.current_path_index]);

                        internode_udp.send_message(next_port, InternodeType::NodeDataResponse, response.to_data());
                    }

                    break;
                }
            }
        }
    }

    return 0;
}
