/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

#ifndef CITS3002_PROJECT_INTERNODE_UDP_H
#define CITS3002_PROJECT_INTERNODE_UDP_H

#include <string>
#include <utility>
#include <vector>
#include <unordered_map>
#include <memory>

#include <poll.h>
#include <sys/socket.h>

enum class InternodeType : uint16_t {
    Undefined = 0,
    ReadyToReceive = 1,
    Greeting = 2,
    NodeDataRequest = 3,
    NodeDataResponse = 4
};

struct InternodeHeader {
    InternodeType internodeType = InternodeType::Undefined;
    uint16_t data_size = 0;
};

struct InternodeMessage {
    InternodeMessage(InternodeHeader header, std::vector<uint8_t> data, sockaddr_storage from_addr)
            : header(header), data(std::move(data)), from_addr(from_addr) {};

    InternodeHeader header;
    std::vector<uint8_t> data;

    [[nodiscard]] std::string get_port_string() const;
private:

    friend class InternodeUDP;

    sockaddr_storage from_addr;
};

struct ReceivedInternode {
    std::vector<InternodeMessage> internode_messages{};
};

class InternodeUDP {
    int socket_fd;

    // Port -> Name
    std::unordered_map<std::string, std::string> neighbour_port_to_name;
    // Name -> Port
    std::unordered_map<std::string, std::string> neighbour_name_to_port;
    // Port -> Unique ID
    std::unordered_map<std::string, uint16_t> neighbour_port_to_unique_id;
    // Name -> Unique ID
    std::unordered_map<std::string, uint16_t> neighbour_name_to_unique_id;
    // Unique ID -> Name
    std::unordered_map<uint16_t, std::string> neighbour_unique_id_to_name;
    // Unique ID -> Port
    std::unordered_map<uint16_t, std::string> neighbour_unique_id_to_port;

    void sync_with_neighbours(const std::string &name, uint16_t unique_id, const std::vector<std::string> &neighbour_ports);

    void send_message(const sockaddr *addr, InternodeType type, const std::vector<uint8_t> &data) const;
    void send_message(const sockaddr *addr, InternodeType type, const uint8_t *data, uint16_t data_size) const;
public:
    explicit InternodeUDP(const std::string &name, uint16_t unique_id, const std::string &port, const std::vector<std::string> &neighbour_ports);
    ~InternodeUDP();

    static constexpr uint16_t HEADER_SIZE = sizeof(InternodeHeader); // Should be 4
    static constexpr uint16_t MAX_PACKET_SIZE = 1500; // Should prevent any fragmentation, and from my understanding unsure all UDP packets are sent in one call to 'sendto'
    static constexpr uint16_t MAX_MESSAGE_SIZE = MAX_PACKET_SIZE - HEADER_SIZE;

    InternodeUDP(InternodeUDP const &) = delete;
    InternodeUDP &operator=(const InternodeUDP &) = delete;

    void send_message(const std::string &port, InternodeType type, const std::vector<uint8_t> &data) const;
    void send_message(const std::string &port, InternodeType type, const uint8_t *data, uint16_t data_size) const;

    void send_message(const InternodeMessage &respond_to, InternodeType type, const std::vector<uint8_t> &data) const;
    void send_message(const InternodeMessage &respond_to, InternodeType type, const uint8_t *data, uint16_t data_size) const;

    pollfd get_poll_struct() const;
    ReceivedInternode process_poll_struct(pollfd poll_struct) const;

    bool has_port_wise_neighbour(const std::string &name) const;
    const std::string &get_neighbour_port(const std::string &name) const;
    const std::string &get_neighbour_name(const std::string &port) const;
    uint16_t get_neighbour_unique_id_from_port(const std::string &port) const;
    uint16_t get_neighbour_unique_id_from_name(const std::string &name) const;
    const std::string &get_neighbour_name_from_uid(uint16_t unique_id) const;
    const std::string &get_neighbour_port_from_uid(uint16_t unique_id) const;
};

#endif //CITS3002_PROJECT_INTERNODE_UDP_H
