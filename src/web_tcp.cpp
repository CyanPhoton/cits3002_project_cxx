/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

#include "web_tcp.h"

#include <sstream>
#include <vector>
#include <cstring>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

#include "toolbox.h"

#define BACKLOG 10
#define TEMP_BUFFER_SIZE 4096

WebTCPListener::WebTCPListener(const std::string &port) {
    addrinfo address_hints{};
    address_hints.ai_family = AF_UNSPEC;
    address_hints.ai_socktype = SOCK_STREAM;
    address_hints.ai_flags = AI_PASSIVE;

    addrinfo *address_info = nullptr;

    EXPECT(getaddrinfo(nullptr, port.c_str(), &address_hints, &address_info) >= 0, "Failed to get address info.")

    socket_fd = socket(address_info->ai_family, address_info->ai_socktype, address_info->ai_protocol);
    EXPECT(socket_fd >= 0, "Failed to create socket fd.")

    int enabled = 1;
    EXPECT(setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &enabled, sizeof(enabled)) >= 0, "Failed to enable address and port reuse on socket.")

    EXPECT(bind(socket_fd, address_info->ai_addr, address_info->ai_addrlen) >= 0, "Failed to bind socket.")

    EXPECT(listen(socket_fd, BACKLOG) >= 0, "Failed to listen on socket.")

    freeaddrinfo(address_info);
}

WebTCPListener::~WebTCPListener() {
    close(socket_fd);
}

pollfd WebTCPListener::get_poll_struct() {
    return pollfd{
            socket_fd,
            POLLIN,
            0
    };
}

std::optional<std::pair<HTTPRequest, WebTCPResponder>> WebTCPListener::process_poll_struct(pollfd poll_struct) {
    if ((poll_struct.revents & POLLIN) == 0) {
        return {};
    }

    sockaddr_storage remote_socket_address{};
    socklen_t remote_socket_len = sizeof(sockaddr_storage);

    int remote_socket_fd = accept(socket_fd, (sockaddr *) &remote_socket_address, &remote_socket_len);
    EXPECT(remote_socket_fd >= 0, "Failed to accept connection on socket.")

    std::vector<char> msg_bytes;
    char temp_buffer[TEMP_BUFFER_SIZE];
    while (true) {
        int bytes_received = recv(remote_socket_fd, temp_buffer, TEMP_BUFFER_SIZE, 0);
        EXPECT(bytes_received >= 0, "Failed to recv on remote socket")

        if (bytes_received == 0) {
            break;
        }

        msg_bytes.insert(msg_bytes.end(), temp_buffer, temp_buffer + bytes_received);

        // By HTTP spec the body starts after double \r\n https://www.w3.org/Protocols/rfc2616/rfc2616-sec5.html
        // and here we only care about the header, and only the header is sent since there is not body
        if (memcmp("\r\n\r\n", &*msg_bytes.end() - 4, 4) == 0) {
            break;
        }
    }

    std::string header_string;
    header_string.append(msg_bytes.data(), msg_bytes.size());
    msg_bytes.clear();

    HTTPRequest http_request{
            header_string
    };

    WebTCPResponder web_tcp_responder{remote_socket_fd};

    return {{http_request, web_tcp_responder}};
}

WebTCPResponder::WebTCPResponder(int remote_socket_fd) : remote_socket_fd(remote_socket_fd) {}

bool WebTCPResponder::send_and_close(const std::string &status, const std::string &body) {
    if (remote_socket_fd == -1) {
        throw std::runtime_error("Must not try to send a response after already closing");
    }

    std::stringstream response;

    response << "HTTP/1.1 " << status << "\r\n";

    response << "Content-Length: " << body.length() << "\r\n";
    response << "Content-Type: text/html\r\n";
    response << "Connection: Closed\r\n";
    response << "\r\n";
    response << body;

    std::string response_string = response.str();

    size_t response_size = response_string.size(); // Don't send null byte?
    size_t total_bytes_sent = 0;

    while (total_bytes_sent < response_size) {
        size_t remaining = response_size - total_bytes_sent;

        int bytes_sent = send(remote_socket_fd, response_string.c_str() + total_bytes_sent, remaining, 0);
        EXPECT(bytes_sent >= 0, "Failed to send on remote socket")

        if (bytes_sent == 0) {
            close(remote_socket_fd);
            remote_socket_fd = -1;

            return false;
        }

        total_bytes_sent += bytes_sent;
    }

    close(remote_socket_fd);
    remote_socket_fd = -1;

    return true;
}

std::string RequestParsing::extract_get_uri(const std::string &header) {
    std::string remain = header;

    std::string method;
    std::tie(method, remain) = split_string(remain, " ");

    if (method != "GET") {
        return "";
    }

    std::string URI;
    std::tie(URI, remain) = split_string(remain, " ");

    return URI;
}

std::unordered_map<std::string, std::string> RequestParsing::extract_get_key_value_pairs(const std::string &uri) {
    std::string pairs;
    std::tie(std::ignore, pairs) = split_string(uri, "/?");

    std::vector<std::string> pair_vec = split_string_all(pairs, "&");

    std::unordered_map<std::string, std::string> result{};

    result.reserve(pair_vec.size());
    for (const auto &pair : pair_vec) {
        result.insert(split_string(pair, "="));
    }

    return result;
}
