/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

#include "internode_udp.h"

#include <unordered_set>
#include <cstring>
#include <netdb.h>
#include <unistd.h>

#include "toolbox.h"

std::string InternodeMessage::get_port_string() const {
    in_port_t port;

    if (from_addr.ss_family == AF_INET) {
        port = ((sockaddr_in *) &from_addr)->sin_port;
    } else {
        port = ((sockaddr_in6 *) &from_addr)->sin6_port;
    }

    return std::to_string(ntohs(port)); // This should be right, need to convert from network to host though
}

InternodeUDP::InternodeUDP(const std::string &name, uint16_t unique_id, const std::string &port, const std::vector<std::string> &neighbour_ports) {
    addrinfo address_hints{};
    address_hints.ai_family = AF_UNSPEC;
    address_hints.ai_socktype = SOCK_DGRAM;
    address_hints.ai_flags = AI_PASSIVE;

    addrinfo *address_info = nullptr;

    EXPECT(getaddrinfo(nullptr, port.c_str(), &address_hints, &address_info) >= 0, "Failed to get address info.")

    socket_fd = socket(address_info->ai_family, address_info->ai_socktype, address_info->ai_protocol);
    EXPECT(socket_fd >= 0, "Failed to create socket fd.")

    int enabled = 1;
    EXPECT(setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &enabled, sizeof(enabled)) >= 0, "Failed to enable address and port reuse on socket.")

    EXPECT(bind(socket_fd, address_info->ai_addr, address_info->ai_addrlen) >= 0, "Failed to bind socket.")

    freeaddrinfo(address_info);

    sync_with_neighbours(name, unique_id, neighbour_ports); // Make sure all neighbours are ready to receive over UDP, and learn names
}

void InternodeUDP::sync_with_neighbours(const std::string &name, uint16_t unique_id, const std::vector<std::string> &neighbour_ports) {
    // Note: assumes lossless UDP, like the entire project

    size_t expected = neighbour_ports.size();

    // Set of ports greeting message has been sent to
    std::unordered_set<std::string> sent_greetings{};

    while (true) {
        // Send RTR to all neighbours, yet to be heard back from
        for (const auto &neighbour_port : neighbour_ports) {
            if (neighbour_port_to_name.count(neighbour_port) == 0) {
                send_message(neighbour_port, InternodeType::ReadyToReceive, {});
            }
        }

        // Wait for a message, or time out
        pollfd poll_struct = get_poll_struct();

        int ready = poll(&poll_struct, 1, 10);
        EXPECT(ready >= 0, "Failed to poll on socket")

        // While there are immediately some messages to receive, process and if needed respond to them
        while (true) {
            auto received = process_poll_struct(poll_struct);

            for (const auto &message : received.internode_messages) {
                switch (message.header.internodeType) {
                    case InternodeType::ReadyToReceive: {

                        std::vector<uint8_t> data{};
                        data.insert(data.end(), (uint8_t *) &unique_id, ((uint8_t *) &unique_id) + sizeof(unique_id));
                        data.insert(data.end(), name.c_str(), name.c_str() + name.size());  // Exclude /0 since size is known

                        send_message(message, InternodeType::Greeting, data.data(), data.size());
                        sent_greetings.insert(message.get_port_string());

                        break;
                    }
                    case InternodeType::Greeting: {

                        const uint16_t *neighbour_unique_id = (uint16_t *) &message.data[0];
                        std::string neighbour_name((char *) message.data.data() + sizeof(uint16_t), message.data.size() - sizeof(uint16_t));
                        std::string neighbour_port = message.get_port_string();

                        neighbour_port_to_unique_id[neighbour_port] = *neighbour_unique_id;
                        neighbour_name_to_unique_id[neighbour_name] = *neighbour_unique_id;
                        neighbour_port_to_name[neighbour_port] = neighbour_name;
                        neighbour_name_to_port[neighbour_name] = neighbour_port;
                        neighbour_unique_id_to_name[*neighbour_unique_id] = neighbour_name;
                        neighbour_unique_id_to_port[*neighbour_unique_id] = neighbour_port;

                        break;
                    }
                    default:
                        ERR_LOG("Got unexpected message type while syncing with neighbours: " << (int) message.header.internodeType << std::endl);
                }
            }

            poll_struct = get_poll_struct();
            ready = poll(&poll_struct, 1, 0);

            if (ready == 0) {
                break;
            }
        }

        // Once we have told all out neighbours our name, and their ours we are synced
        if (sent_greetings.size() == expected && neighbour_name_to_port.size() == expected) {
            break;
        }
    }

    LOG(name << " is now synced");
}

InternodeUDP::~InternodeUDP() {
    close(socket_fd);
}

void InternodeUDP::send_message(const std::string &port, InternodeType type, const std::vector<uint8_t> &data) const {
    EXPECT(data.size() <= MAX_MESSAGE_SIZE, "Messages have a max data size of " << MAX_MESSAGE_SIZE << " bytes, tried: " << data.size() << " bytes, of type: " << (int) type)

    send_message(port, type, data.data(), data.size());
}

void InternodeUDP::send_message(const std::string &port, InternodeType type, const uint8_t *data, uint16_t data_size) const {
    EXPECT(data_size <= MAX_MESSAGE_SIZE, "Messages have a max data size of " << MAX_MESSAGE_SIZE << " bytes, tried: " << data_size << " bytes, of type: " << (int) type)

    addrinfo address_hints{};
    address_hints.ai_family = AF_UNSPEC;
    address_hints.ai_socktype = SOCK_DGRAM;

    addrinfo *address_info = nullptr;

    // Here is were we assume other nodes are on same computer
    EXPECT(getaddrinfo("localhost", port.c_str(), &address_hints, &address_info) >= 0, "Failed to get address info for port: " << port)

    send_message(address_info->ai_addr, type, data, data_size);

    freeaddrinfo(address_info);
}

void InternodeUDP::send_message(const InternodeMessage &respond_to, InternodeType type, const std::vector<uint8_t> &data) const {
    EXPECT(data.size() <= MAX_MESSAGE_SIZE, "Messages have a max data size of " << MAX_MESSAGE_SIZE << " bytes, tried: " << data.size() << " bytes, of type: " << (int) type)

    send_message((sockaddr *) &respond_to.from_addr, type, data);
}

void InternodeUDP::send_message(const InternodeMessage &respond_to, InternodeType type, const uint8_t *data, uint16_t data_size) const {
    EXPECT(data_size <= MAX_MESSAGE_SIZE, "Messages have a max data size of " << MAX_MESSAGE_SIZE << " bytes, tried: " << data_size << " bytes, of type: " << (int) type)

    send_message((sockaddr *) &respond_to.from_addr, type, data, data_size);
}

void InternodeUDP::send_message(const sockaddr *addr, InternodeType type, const std::vector<uint8_t> &data) const {
    EXPECT(data.size() <= MAX_MESSAGE_SIZE, "Messages have a max data size of " << MAX_MESSAGE_SIZE << " bytes, tried: " << data.size() << " bytes, of type: " << (int) type)

    send_message(addr, type, data.data(), data.size());
}

void InternodeUDP::send_message(const sockaddr *addr, InternodeType type, const uint8_t *data, uint16_t data_size) const {
    EXPECT(data_size <= MAX_MESSAGE_SIZE, "Messages have a max data size of " << MAX_MESSAGE_SIZE << " bytes, tried: " << data_size << " bytes, of type: " << (int) type)

    std::vector<uint8_t> bytes{};
    bytes.reserve(HEADER_SIZE + data_size);

    InternodeHeader header{
            (InternodeType) htons((uint16_t) type),
            htons(data_size),
    };
    bytes.insert(bytes.end(), (uint8_t *) &header, ((uint8_t *) &header) + HEADER_SIZE);

    if (data != nullptr && data_size != 0) {
        bytes.insert(bytes.end(), data, data + data_size);
    }

    EXPECT(((InternodeHeader *) bytes.data())->internodeType == (InternodeType) htons((uint16_t) type), "Header was not translated properly")
    EXPECT(((InternodeHeader *) bytes.data())->data_size == htons(data_size), "Header was not translated properly")

//    LOG("Sending: " << bytes.size() << " bytes" << std::endl);
    size_t bytes_sent = sendto(socket_fd, bytes.data(), bytes.size(), 0, addr, sizeof(sockaddr_storage));
    EXPECT(bytes_sent >= 0, "Failed to sendto on socket")
    EXPECT(bytes_sent == bytes.size(), "Expected to send entire pack over UDP in send one call")
    // From my understanding, when using blocking send over UDP, it will either send the entire packet or block, never partial
    // So long as the sndbuf is big enough, which is should most likely be
}

pollfd InternodeUDP::get_poll_struct() const {
    return pollfd{
            socket_fd,
            POLLIN,
            0
    };
}

ReceivedInternode InternodeUDP::process_poll_struct(pollfd poll_struct) const {
    if ((poll_struct.revents & POLLIN) == 0) {
        return {};
    }

    ReceivedInternode received_internode{};
    uint8_t write_buffer[MAX_PACKET_SIZE];

    while (true) {
        sockaddr_storage from_addr{};
        socklen_t from_addr_len = sizeof(from_addr);

        size_t bytes_received = recvfrom(socket_fd, write_buffer, MAX_MESSAGE_SIZE, 0, (sockaddr *) &from_addr, &from_addr_len);
        EXPECT(bytes_received >= 0, "Failed to recvfrom on socket.")

        auto *header = (InternodeHeader *) write_buffer;
        header->internodeType = (InternodeType) ntohs((uint16_t) header->internodeType);
        header->data_size = ntohs(header->data_size);

//        LOG("Header size: " << HEADER_SIZE << " | Data Size: " << header->data_size << " | Received: " << bytes_received << " | Type:" << (int) header->internodeType << std::endl);
        EXPECT(header->data_size == bytes_received - HEADER_SIZE, "Should always receive entire message in one call to recvfrom, perhaps invalid data send?")

        std::vector<uint8_t> data;
        data.insert(data.end(), write_buffer + HEADER_SIZE, write_buffer + bytes_received);

        received_internode.internode_messages.emplace_back(*header, data, from_addr);

        pollfd new_poll_struct = get_poll_struct();

        int avail = poll(&new_poll_struct, 1, 0);
        if (avail == 0) {
            break;
        }
    }

    return received_internode;
}

bool InternodeUDP::has_port_wise_neighbour(const std::string &name) const {
    return neighbour_name_to_port.count(name) != 0;
}

const std::string &InternodeUDP::get_neighbour_port(const std::string &name) const {
    return neighbour_name_to_port.at(name);
}

const std::string &InternodeUDP::get_neighbour_name(const std::string &port) const {
    return neighbour_port_to_name.at(port);
}

uint16_t InternodeUDP::get_neighbour_unique_id_from_port(const std::string &port) const {
    return neighbour_port_to_unique_id.at(port);
}

uint16_t InternodeUDP::get_neighbour_unique_id_from_name(const std::string &name) const {
    return neighbour_name_to_unique_id.at(name);
}

const std::string &InternodeUDP::get_neighbour_name_from_uid(uint16_t unique_id) const {
    return neighbour_unique_id_to_name.at(unique_id);
}

const std::string &InternodeUDP::get_neighbour_port_from_uid(uint16_t unique_id) const {
    return neighbour_unique_id_to_port.at(unique_id);
}
