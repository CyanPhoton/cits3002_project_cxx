/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

#ifndef CITS3002_PROJECT_UNIQUE_LOW_PRIORITY_QUEUE_H
#define CITS3002_PROJECT_UNIQUE_LOW_PRIORITY_QUEUE_H

#include <unordered_map>
#include <unordered_set>
#include <set>
#include <map>

// Puts smallest priority 'on top'
template<typename Priority, typename Value>
class unique_low_priority_queue {
    std::map<Priority, std::unordered_set<Value>> per_priority_value_set{};
    std::unordered_map<Value, Priority> value_priority_lookup{};
public:
    unique_low_priority_queue() = default;

    [[nodiscard]] Value top_value() const;
    [[nodiscard]] Priority top_priority() const;
    [[nodiscard]] std::pair<Priority, Value> top_pair() const;

    [[nodiscard]] bool empty() const;
    [[nodiscard]] size_t size() const;

    // Returns true if the values was actually inserted,
    // else false if the value already exists but with a higher priority
    bool push(const Priority &priority, const Value &value);

    void pop();
};

template<typename Priority, typename Value>
Value unique_low_priority_queue<Priority, Value>::top_value() const {
    return *per_priority_value_set.cbegin()->second.cbegin();
}

template<typename Priority, typename Value>
Priority unique_low_priority_queue<Priority, Value>::top_priority() const {
    return per_priority_value_set.cbegin()->first;
}

template<typename Priority, typename Value>
std::pair<Priority, Value> unique_low_priority_queue<Priority, Value>::top_pair() const {
    return {top_priority(), top_value()};
}

template<typename Priority, typename Value>
bool unique_low_priority_queue<Priority, Value>::empty() const {
    return value_priority_lookup.empty();
}

template<typename Priority, typename Value>
size_t unique_low_priority_queue<Priority, Value>::size() const {
    return value_priority_lookup.size();
}

template<typename Priority, typename Value>
bool unique_low_priority_queue<Priority, Value>::push(const Priority &priority, const Value &value) {

    if (value_priority_lookup.count(value) != 0) {
        // Value already exists

        // Get associated priority
        Priority old_p = value_priority_lookup.at(value);

        if (old_p < priority) {
            // Current one already has a lower priority, so don't insert
            return false;
        }

        // Remove old lookup
        value_priority_lookup.erase(value);

        // Remove old value
        per_priority_value_set.at(old_p).erase(value);

        // Remove priority set if empty
        if (per_priority_value_set.at(old_p).empty()) {
            per_priority_value_set.erase(old_p);
        }
    }

    // add new lookup
    value_priority_lookup[value] = priority;

    // Add new value
    per_priority_value_set[priority].insert(value);

    return true;
}

template<typename Priority, typename Value>
void unique_low_priority_queue<Priority, Value>::pop() {
    std::pair<Priority, Value> top = top_pair();

    // Remove value from set
    per_priority_value_set.at(top.first).erase(top.second);

    // Remove set if empty
    if (per_priority_value_set.at(top.first).empty()) {
        per_priority_value_set.erase(top.first);
    }

    // Remove lookup
    value_priority_lookup.erase(top.second);
}

#endif //CITS3002_PROJECT_UNIQUE_LOW_PRIORITY_QUEUE_H
