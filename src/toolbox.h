/*
 *   Name, Student Number
 *
 *   Matthew Chidlow, 2248 2835
 */

#ifndef CITS3002_PROJECT_TOOLBOX_H
#define CITS3002_PROJECT_TOOLBOX_H

#include <iostream>
#include <tuple>
#include <vector>
#include <cstring>

#include "timetable.h"

extern const char *server_name;
#define SET_SERVER_NAME(name) server_name = (name)
#define LOG(msg) std::cout << "[" << server_name << ":MSG] " << msg
#define ERR_LOG(msg) std::cerr << "[" << server_name << ":ERROR] " << msg

#define PANIC(msg) ERR_LOG("[" << __FILE__ << ":" << __LINE__ << "] " << msg << std::endl); exit(EXIT_FAILURE);
#define EXPECT(result, msg) \
if (!(result)) {\
    ERR_LOG("Errno: " << errno << " | " << strerror(errno) << std::endl);\
    PANIC(msg)\
}

std::pair<std::string, std::string> split_string(const std::string &input, const std::string &delim);

std::vector<std::string> split_string_all(const std::string &input, const std::string &delim);

uint16_t get_now_time_int();
uint16_t clock_24h_time_to_int(const std::string &clock_time);
std::string int_to_clock_12h_time(uint16_t int_time);

std::string form_response_immediate(const TimetableEntry &first_timetable, uint16_t request_time);
std::string form_response_general(const TimetableEntry &first_timetable, uint16_t request_time, uint16_t arrival_time, const std::string &destination_name, uint16_t day_jumps);

#endif //CITS3002_PROJECT_TOOLBOX_H
